// create insertOne Method

db.rooms.insertOne({
	name: "single",
	accomodates: 2.0,
	price: 1000.00,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

// create insertMany Method

db.rooms.insertMany([
{
	 name: "double",
	 accomodates: 2.0,
	 price: 2000.00,
	 description: "A room fit for a small family going on a vacation",
	 rooms_available: 5.0,
	 isAvailable: false
},{
	 name: "queen",
	 accomodates: 4.0,
	 price: 4000.00,
	 description: "A room with a queen sized bed perfect for a simple getaway",
	 rooms_available: 15.0,
	 isAvailable: false
}

]);

//create find method

db.rooms.find({
	name: "Double"
});

//create updateOne method

db.rooms.updateOne({
	name: "queen"
},{
	$set: {
		rooms_available: 0
	}
})

//create deleteMany

db.rooms.deleteMany({
	rooms_available: 0
})